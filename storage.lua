-- This file defines the functions needed for controlling global values.
--
-- The base format is get/set_foo(pos, name)
--                                 ^ pos is not needed always needed.

local mod_storage = minetest.get_mod_storage()

-- Navigation
function tardis.set_nav(pos, name)
	local pos_string = (minetest.pos_to_string(pos))

	if (pos_string) then
		if (mod_storage:set_string("tardis:"..name..":destination", pos_string)) then
			return true
		else return false
		end
	end
end

function tardis.get_nav(name)
	local pos = minetest.string_to_pos(mod_storage:get_string("tardis:"..name..":destination"))

	if (pos) then
		return pos
	elseif (tardis.get_exterior(name)) then
		return tardis.get_exterior(name)
	else return { x = 0, y = 0, z = 0 }
	end
end

-- Exterior Position
function tardis.set_exterior(pos, name)
	local pos_string = (minetest.pos_to_string(pos))

	if (pos_string) then
		if (mod_storage:set_string("tardis:"..name..":exterior", pos_string)) then
			return true
		else return false
		end
	end
end

function tardis.get_exterior(name)
	local pos = minetest.string_to_pos(mod_storage:get_string("tardis:"..name..":exterior"))

	if (pos) then
		return pos
	else return false
	end
end

-- Position of interior doors
function tardis.set_interior(pos, name)
	local pos_string = (minetest.pos_to_string(pos))

	if (pos_string) then
		if (mod_storage:set_string("tardis:"..name..":interior", pos_string)) then
			return true
		else return false
		end
	end
end

function tardis.get_interior(name)
	local pos = minetest.string_to_pos(mod_storage:get_string("tardis:"..name..":interior"))

	if (pos) then
		return pos
	else return false
	end
end

-- Number of TARDISes currently spawned, used for calculating interior position.
function tardis.add_count()
	local current_count = tonumber(mod_storage:get_string("tardis:count"))

	if (current_count) then
		if (mod_storage:set_string("tardis:count", tostring(current_count+1))) then
			return true
		else return false
		end

	elseif (mod_storage:set_string("tardis:count", "1")) then
		return true
	else return false
	end
end

function tardis.get_count()
	local current_count = tonumber(mod_storage:get_string("tardis:count"))

	if (current_count) then
		return current_count
	else return 0
	end
end

-- Whether dematerialized or not
function tardis.set_vortex(bool, name)
	if (mod_storage:set_string("tardis:"..name..":vortex", tostring(bool))) then
		return true
	else return nil
	end
end

function tardis.get_vortex(name)
	if (mod_storage:get_string("tardis:"..name..":vortex") == "true") then
		return true
	else return false
	end
end

-- Export Mod Storage To Table (used for looping though pairs()
function tardis.to_table()
	return mod_storage:to_table()
end
