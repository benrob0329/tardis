-- Define global table
tardis = {}

local modname   = minetest.get_current_modname()
local modpath   = minetest.get_modpath(modname)
local gravity   = tonumber(core.settings:get("movement_gravity")) or 9.81

minetest.register_entity("tardis:lamp", {
	physical = false,
	visual = "sprite",
	visual_size = {x=1, y=1},
	use_texture_alpha = true,
	textures = {"tardis_lamp.png"},
	glow = 14,
})

minetest.register_entity("tardis:tardis", {
	hp_max = 1,
	physical = true,
	weight = 5,
	visual = "mesh",
	visual_size = {x=11, y=11, z=11},
	mesh = "tardis_exterior.obj",
	use_texture_alpha = true,
	textures = {"(tardis_exterior.png)"},
	backface_culling = true,

	on_step = function(self, dtime)
		self.object:set_acceleration({x=0, y=-gravity, z=0})
	end,

	set_alpha = function(self, alpha)
		if not tonumber(alpha) then
			return false
		end

		alpha = math.floor(tonumber(alpha))
		local tex = self.object:get_properties().textures
		local base = tex[1]:match("%(.*%)")
		local mod = "^[opacity:" .. alpha
		if alpha >= 255 then
			tex[1] = base
		elseif alpha <= 0 then
			tex[1] = base .. "^[opacity:0"
		else
			tex[1] = base..mod
		end

		self.object:set_properties({textures = tex})
	end,

	lamp = function(self, bool)
		local pos = self.object:get_pos()
		pos.y = pos.y + 2

		if bool then
			self.lamp_entity = minetest.add_entity(pos, "tardis:lamp")

			--local lamp_data = self.lamp_entity:get_luaentity()
			--lamp_data.glow = 14
			self.lamp_entity:set_attach(self.object, "", {x=0, y=1.8, z=0}, {x=0, y=0, z=0})
		else
			if self.lamp_entity then
				self.lamp_entity:remove()
				self.lamp_entity = nil
			end
		end
	end,

	fade = function(self, i, ider, time)
		if i < 256 then
			self:set_alpha(i)
			i = i+ider
			minetest.after(time, self.fade, self, i, ider, time)
		else
			self:set_alpha(255)
		end
	end,

	remat = function(self)
		self:set_alpha(0)
		minetest.after(12, self.fade, self, 0, 2, 0.04)


		for i=11,18 do
		   minetest.after(i, function(self, i)
				     if i%2 == 0 then
					self:lamp(false)
				     else
					self:lamp(true)
				     end
				     end, self, i)
		end

		   minetest.after(18.5, function(self)
				     self:lamp(false)
					end, self)

		minetest.sound_play("tardis_remat", {
			pos = self.object:get_pos(),
			max_hear_distance = 10,
			gain = 1.0,
		})
	end,


	on_rightclick = function(self, clicker)
		self:remat()
	end,
})
